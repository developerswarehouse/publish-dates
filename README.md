# Publish Dates for your eloquent models

I have found with my recent projects, that I'm using the same functionality for _publishing_ content.  So, I made a package.

## Installation
    composer require developerswarehouse/publish-dates

## How it works
Say, you're working with an `Article` model, we might want to use publishing dates to manage the displaying of the content.

On your models, you'll want to add two fields for storing the publish dates.

    publish_from(timestamp:nullable)
    publish_to(timestamp:nullable)

If you want to change the name of the fields you can change the property in your model.

    protected $publishFields = [
        'publish_from',
        'publish_to',
    ];

Or; overwrite the functions for `getPublishedFrom` and `getPublishedOn` with the field name alternatives.

## Usage
On your Laravel models, add the trait for publish dates.

    use DevelopersWarehouse\PublishDates\PublishDates;

    class Article extends Model
    {
        use PublishDates;
    }


Then, in your controller you can filter the `Articles` by:

    public function index() {
        $articles = Article::published()->get();

        return view('articles.index', compact("articles"));
    }

If you want to fetch `Articles` that are yet to be published, you can use.

    $articles = Article::unpublished()->get();

# Further Developments
I will be looking to extend the functionality to make this more versatile and something you might want to use.
