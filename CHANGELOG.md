# Release Notes

## 1.0.3 (2018-01-21)
Added further scope query methods for filtering.

## 1.0.2 (2018-01-14)
Rebrand OnPoint to DevelopersWarehouse.

## 1.0.1 (2017-12-13)
Added installation notes, and change log.

## 1.0 (2017-12-12)
Initial creation of package
