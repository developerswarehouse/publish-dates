<?php

namespace DevelopersWarehouse\PublishDates;

use Datetime;

trait PublishDates {

    protected $publishFields = [
        'publish_from',
        'publish_to',
    ];

    /**
     * Get the 'from_date' field.
     * @return [type] [description]
     */
    public function getPublishedFrom()
    {
        return $this->publishFields[0];
    }

    /**
     * Get the 'to_date' field.
     * @return [type] [description]
     */
    public function getPublishedTo()
    {
        return $this->publishFields[1];
    }

    /**
     * A scope query to filtering results with published date ranges.
     * @param  [type] $q [description]
     * @return [type]    [description]
     */
    public function scopePublished($q)
    {
        $now = new DateTime();
        return $q->where(function ($q) use($now) {
            $q->where($this->getPublishedFrom(), "<", $now)->orWhereNull($this->getPublishedFrom());
        })->where(function ($q) use($now) {
            $q->where($this->getPublishedTo(), ">", $now)->orWhereNull($this->getPublishedTo());
        });
    }

    /**
     * A scope query to filtering results that fall outside of published ranges.
     * @param  [type] $q [description]
     * @return [type]    [description]
     */
    public function scopeNotPublished($q)
    {
        $now = new DateTime();
        return $q->where(function ($q) use($now) {
            $q->where($this->getPublishedFrom(), ">", $now);
        })->orWhere(function ($q) use($now) {
            $q->where($this->getPublishedTo(), "<", $now);
        });
    }

    /**
     * A scope query for filtering results that will be published since a given date.
     * @param  [type] $q     [description]
     * @param  string $value [description]
     * @return [type]        [description]
     */
    public function scopePublishedFrom($q, $value = 'now')
    {
        $date = new DateTime($value);
        return $q->where(function ($q) use($date) {
            $q->where($this->getPublishedFrom(), ">", $date)->orWhereNull($this->getPublishedFrom());
        });
    }

    /**
     * A scope query for filtering results that will be published till a given date.
     * @param  [type] $q     [description]
     * @param  string $value [description]
     * @return [type]        [description]
     */
    public function scopePublishedTo($q, $value = '+1 minute')
    {
        $date = new DateTime($value);
        return $q->where(function ($q) use($date) {
            $q->where($this->getPublishedTo(), "<", $date)->orWhereNull($this->getPublishedTo());
        });
    }

    /**
     * Query the models where the published dates have availabilty between two dates.
     * @param  [string] $from String parsable by DateTime()
     * @param  [string] $to   String parsable by DateTime()
     */
    public function scopePublishedBetween($q, $from, $to)
    {
        $from = new DateTime($from);
        $to = new DateTime($to);
        return $q->where(function ($q) use($from) {
            $q->where($this->getPublishedFrom(), "<", $from)->orWhereNull($this->getPublishedFrom());
        })->where(function ($q) use($to) {
            $q->where($this->getPublishedTo(), ">", $to)->orWhereNull($this->getPublishedTo());
        });
    }
}
